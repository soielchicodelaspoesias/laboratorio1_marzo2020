import random

# crear matriz
def crear_matriz(matriz):
    for i in range(31):
        matriz.append([0] * 4)
    for i in range(31):
        for j in range(4):
            matriz[i][j] = round(random.uniform(1, 7), 1)
# imprimir matriz
def imprimir_matriz(matriz):
    num = 1
    for i in matriz:
        print("estudiante", num, i)
        num +=1
# calcula el promedio y dice que aprobo o reprovo
def promedio(matriz):
    aprueba = 0
    reprueba = 0
    n = 1
    for i in matriz:
        promedio = 0
        promedio = sum(i)
        promedio = round(promedio / 4, 1)
        if promedio >= 4:
            print("el estudiante", n, "tiene un promedio: ", promedio, ", aprobo")
            aprueba += 1
        else:
            print("el estudiante", n, "tiene un promedio: ", promedio, ", reprobo")
            reprueba += 1
        n += 1
    print("la cantidad de estudiantes que aprobaron son: ", aprueba)
    print("la cantidad de estudiantes que reprobaron son: ", reprueba)

# llama las funciones
matriz = []
crear_matriz(matriz)
imprimir_matriz(matriz)
promedio(matriz)