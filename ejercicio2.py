import random

# crear matriz
def crear_matriz(matriz):
    for i in range(15):
        matriz.append([0] * 15)
    for i in range(15):
        for j in range(12):
            matriz[i][j] = (random.randrange(-10, 10))
# imprimir matriz
def imprimir_matriz(matriz):
    for i in matriz:
        print(i)

# identifica el numero menor
def numero_menor(matriz):
    """ el valor minimo es 10 ya que todos
    los numeros van a ser igual o menor a 10 """
    valor_min = 10
    for i in range(15):
        for j in range(12):
            if matriz[i][j] < valor_min:
                valor_min = matriz[i][j]
    print("el valor minimo es:", valor_min)

# suma los elemtos de las 5 primeras columnas
def suma_columnas(matriz):
    valor_colum1 = 0
    for i in range(15):
        for j in range(1):
            valor_colum1 += matriz[i][j]
    print("el valor de la suma de los elementos de la columna 1:", valor_colum1)

    valor_colum2 = 0
    for i in range(15):
        for j in range(2):
            valor_colum2 += matriz[i][j]
    print("el valor de la suma de los elementos de la columna 2:", valor_colum2)

    valor_colum3 = 0
    for i in range(15):
        for j in range(3):
            valor_colum3 += matriz[i][j]
    print("el valor de la suma de los elementos de la columna 3:", valor_colum2)

    valor_colum4 = 0
    for i in range(15):
        for j in range(4):
            valor_colum4 += matriz[i][j]
    print("el valor de la suma de los elementos de la columna 4:", valor_colum4)

    valor_colum5 = 0
    for i in range(15):
        for j in range(5):
            valor_colum5 += matriz[i][j]
    print("el valor de la suma de los elementos de la columna 5:", valor_colum5)

# identifica la cantidad de negativos que hay entre la columna 5 a la 9
def numeros_negativos(matriz):
    num_negativos = 0
    for i in range(15):
        for j in range(6, 9):
            if matriz[i][j] < 0:
                num_negativos += 1
    print("los la cantidad de numeros negativos de la columna 5 a la 9 es:", num_negativos)


matriz = []
crear_matriz(matriz)
imprimir_matriz(matriz)
numero_menor(matriz)
suma_columnas(matriz)
numeros_negativos(matriz)
